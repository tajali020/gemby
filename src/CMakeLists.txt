file(GLOB_RECURSE QML_SRCS *.qrc *.js *.png *.svg)

set(CPP_FILES
    backend/authentication.cpp
    backend/song.cpp
    backend/databasemanager.cpp
    backend/models/servermodel.cpp
    backend/models/serverlist.cpp
    )

add_executable(${PROJECT_NAME} main.cpp ${QML_SRCS} ${CPP_FILES})

target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Gui Qt5::Qml Qt5::Quick Qt5::QuickControls2 Qt5::Svg Qt5::Multimedia Qt5::Sql)
target_link_libraries(${PROJECT_NAME} KF5::I18n)

install(TARGETS ${PROJECT_NAME} ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
