#ifndef SONG_H
#define SONG_H

#include <QObject>


class Song: public QObject
{
    Q_OBJECT

public:
    Song();
    ~Song() = default;

    Q_INVOKABLE void play();

    QString getURI();
};

#endif // SONG_H
