#include <QStandardPaths>
#include <QDir>
#include <QDebug>

#include "databasemanager.h"

DatabaseManager::DatabaseManager()
{
    dbPath = QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)).absolutePath() + "/db.sql";
}

bool DatabaseManager::connectToDatabase()
{
    if(!QFile(dbPath).exists()){
        return this->createDatabase();
    } else {
        return this->openDatabase();
    }
}

bool DatabaseManager::createDatabase()
{
    if(this->openDatabase()){
        return (this->createTables()) ? true : false;
    } else {
        qDebug() << "Failed to create the database";
        return false;
    }
    return false;
}

bool DatabaseManager::openDatabase()
{
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setHostName("GembyDatabase");
    database.setDatabaseName(dbPath);
    if(!database.open()) {
        qDebug() << "Error: connection with database failed";
        return false;
    }
    else {
        qDebug() << "Database: connection ok, opened " << dbPath;
        return true;
    }

}

void DatabaseManager::closeDatabase()
{
    database.close();
}

bool DatabaseManager::createTables()
{
    bool success = false;
    // you should check if args are ok first...
    if (database.tables(QSql::AllTables).contains( QLatin1String("server")))
        success = true;
    else {
        QSqlQuery query;
        query.prepare("CREATE TABLE server (serverId TEXT NOT NULL, userId TEXT NOT NULL, accessToken TEXT NOT NULL, deviceId TEXT NOT NULL, url TEXT NOT NULL,"
            "PRIMARY KEY(serverId, userId, deviceId));");
        if(query.exec())
        {
            success = true;
        }
        else
        {
             qDebug() << "createTables error:" << query.lastError();
        }
    }
    return success;
}

bool DatabaseManager::addServer(const QString& serverId, const QString& userId, const QString& accessToken, const QString& deviceId, const QString& url)
{
   bool success = false;
   // you should check if args are ok first...
   QSqlQuery query;
   query.prepare("INSERT INTO server (serverId, userId, accessToken, deviceId, url) VALUES (:serverId, :userId, :accessToken, :deviceId, :url);");
   query.bindValue(":serverId", serverId);
   query.bindValue(":userId", userId);
   query.bindValue(":accessToken", accessToken);
   query.bindValue(":deviceId", deviceId);
   query.bindValue(":url", url);
   if(query.exec())
   {
       success = true;
   }
   else
   {
        qDebug() << "addServer error:" << query.lastError();
   }

   return success;
}

void DatabaseManager::printAllServers()
{
    QSqlQuery query("SELECT * FROM server");
    QSqlRecord record = query.record();
    int serverId = record.indexOf("serverId");
    int userId = record.indexOf("userId");
    int AccessToken = record.indexOf("AccessToken");
    int deviceId = record.indexOf("deviceId");
    int _url = record.indexOf("url");
    while (query.next())
    {
        QString server = query.value(serverId).toString();
        QString user = query.value(userId).toString();
        QString token = query.value(AccessToken).toString();
        QString device = query.value(deviceId).toString();
        QString url = query.value(_url).toString();
        qDebug() << url << ": " << server << " " << user << " " << token << " " << device;
    }
}
