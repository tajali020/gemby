#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>

class DatabaseManager
{
public:
    DatabaseManager();
    bool connectToDatabase();
    bool createDatabase();
    bool openDatabase();
    void closeDatabase();

    bool addServer(const QString& server, const QString& userId, const QString& accessToken, const QString& deviceId, const QString& url);
    void printAllServers();

private:
    QSqlDatabase database;
    bool createTables();
    QString dbPath;
};

#endif // DATABASEMANAGER_H
