#include "servermodel.h"

#include "serverlist.h"

ServerModel::ServerModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_List(nullptr)
{
}

/*
QVariant Server::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}
*/

int ServerModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !m_List)
        return 0;

    return m_List->servers().size();
}

QVariant ServerModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const ServerItem item = m_List->servers().at(index.row());
    switch(role) {
    case ServerRole:
        return QVariant(item.serverId);
    case UserRole:
        return QVariant(item.userId);
    case AccessTokenRole:
        return QVariant(item.accessToken);
    case DeviceRole:
        return QVariant(item.deviceId);
    case UrlRole:
        return QVariant(item.url);
    }

    return QVariant();
}

bool ServerModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!m_List)
        return false;

    ServerItem item = m_List->servers().at(index.row());
    switch (role) {
    case ServerRole:
        item.serverId = value.toString();
        break;
    case UserRole:
        item.userId = value.toString();
        break;
    case AccessTokenRole:
        item.accessToken = value.toString();
        break;
    case DeviceRole:
        item.deviceId = value.toString();
        break;
    case UrlRole:
        item.url = value.toString();
        break;
    }

    if (m_List->setItemAt(index.row(), item)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ServerModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}
QHash<int, QByteArray> ServerModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[ServerRole] = "server";
    names[UserRole] = "user";
    names[AccessTokenRole] = "accessToken";
    names[DeviceRole] = "device";
    names[UrlRole] = "url";
    return names;
}

ServerList *ServerModel::list() const
{
    return m_List;
}

void ServerModel::setList(ServerList *list)
{
    beginResetModel();

    if (m_List)
        m_List->disconnect(this);

    m_List = list;

    if (m_List) {
        connect(m_List, &ServerList::preServerAppended, this, [=]() {
            const int index = m_List->servers().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(m_List, &ServerList::postServerAppended, this, [=]() {
            endInsertRows();
        });
        connect(m_List, &ServerList::preServerRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(m_List, &ServerList::postServerRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}
