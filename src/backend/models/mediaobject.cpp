#include "mediaobject.h"

MediaObject::MediaObject(QObject *parent)
    : QAbstractItemModel(parent)
{
}

QModelIndex MediaObject::index(int row, int column, const QModelIndex &parent) const
{
    // FIXME: Implement me!
}

QModelIndex MediaObject::parent(const QModelIndex &index) const
{
    // FIXME: Implement me!
}

int MediaObject::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

int MediaObject::columnCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

QVariant MediaObject::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}
