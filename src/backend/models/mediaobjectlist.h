#ifndef MEDIAOBJECTLIST_H
#define MEDIAOBJECTLIST_H

#include <QObject>

struct MediaObjectItem
{
    QString path;
    QString type;
    bool fav;
};

class MediaObjectList : public QObject
{
    Q_OBJECT
public:
    explicit MediaObjectList(QObject *parent = nullptr);

signals:

};

#endif // MEDIAOBJECTLIST_H
