#ifndef SERVERLIST_H
#define SERVERLIST_H

#include <QObject>
#include <QVector>

struct ServerItem
{
    QString serverId;
    QString userId;
    QString accessToken;
    QString deviceId;
    QString url;
};

class ServerList : public QObject
{
    Q_OBJECT
public:
    explicit ServerList(QObject *parent = nullptr);

    QVector<ServerItem> servers() const;

    bool setItemAt(int index, const ServerItem &item);

signals:
    void preServerAppended();
    void postServerAppended();

    void preServerRemoved(int index);
    void postServerRemoved();

public slots:
    void appendServer(ServerItem item);
    void removeServer(int index);

private:
    QVector<ServerItem> m_Servers;
};

#endif // SERVERLIST_H
