#ifndef SERVERMODEL_H
#define SERVERMODEL_H

#include <QAbstractListModel>

class ServerList;

class ServerModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(ServerList *list READ list WRITE setList)

public:
    explicit ServerModel(QObject *parent = nullptr);

    enum {
        ServerRole = Qt::UserRole,
        UserRole,
        AccessTokenRole,
        DeviceRole,
        UrlRole
    };

    // Header:
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    ServerList *list() const;
    void setList(ServerList *list);

private:
    ServerList *m_List;
};

#endif // SERVERMODEL_H
