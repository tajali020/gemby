#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include "serverlist.h"
#include "../databasemanager.h"

ServerList::ServerList(QObject *parent) : QObject(parent)
{
    // [TODO] Not a permanent solution. VERY temporary
    DatabaseManager db = DatabaseManager();
    if (db.connectToDatabase()) {
        QSqlQuery query("SELECT * FROM server");
        QSqlRecord record = query.record();
        int serverId = record.indexOf("serverId");
        int userId = record.indexOf("userId");
        int AccessToken = record.indexOf("AccessToken");
        int deviceId = record.indexOf("deviceId");
        int _url = record.indexOf("url");
        while (query.next())
        {
            QString server = query.value(serverId).toString();
            QString user = query.value(userId).toString();
            QString token = query.value(AccessToken).toString();
            QString device = query.value(deviceId).toString();
            QString url = query.value(_url).toString();
            m_Servers.append({server,user,token,device,url});
        }
        db.closeDatabase();
    }
}

QVector<ServerItem> ServerList::servers() const
{
    return m_Servers;
}

bool ServerList::setItemAt(int index, const ServerItem &item)
{
    if (index <0 || index >= m_Servers.size())
        return false;
    const ServerItem &oldItem = m_Servers.at(index);
    if (item.serverId == oldItem.serverId && item.accessToken == oldItem.accessToken)
        return false;
    m_Servers[index] = item;
    return true;
}

void ServerList::appendServer(ServerItem item)
{
    emit preServerAppended();

    m_Servers.append(item);

    emit postServerAppended();
}

void ServerList::removeServer(int index)
{
    if (index < m_Servers.size()) {
        emit preServerRemoved(index);

        m_Servers.removeAt(index);

        emit postServerRemoved();
    }
}
