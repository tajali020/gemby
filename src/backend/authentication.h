#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <QObject>
#include <QNetworkReply>

class Authentication: public QObject {
    Q_OBJECT

public:
    Authentication();
    ~Authentication() = default;

    Q_INVOKABLE void login(QString url, QString username, QString password);

    QString getUrl() {return url;};
    void setUrl(QString url) {this->url = url;};

    QString getUsername() {return username;};
    void setUsername(QString username) {this->username = username;};

    QString getPassword() {return password;};
    void setPassword(QString password) {this->password=password;};

private slots:
    void doLogin(QNetworkReply*);

private:
    QString generateDeviceId();
    QString url;
    QString username;
    QString password;
};

#endif // AUTHENTICATION_H
