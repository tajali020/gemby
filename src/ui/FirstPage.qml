import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import "./convergence-components/platform"
import "./convergence-components"
import "./components"

PagePL {
    id: page
    title: "Gemby"

    pageMenu: PageMenuPL {

        PageMenuItemPL {
            text: app.tr("View")
            iconName: "view-list-icons"
        }
        PageMenuItemPL {
            text: app.tr("Home")
            onTriggered: showPassiveNotification(app.tr("Action 2 clicked"))

        }
        PageMenuItemPL {
            text: app.tr("Music")
            onTriggered: app.pages.push("qrc:///MusicPage.qml")
        }
        PageMenuItemPL {
            text: app.tr("Settings")
            onTriggered: app.pages.push("qrc:///SettingsPage.qml")
        }
        PageMenuItemPL {
            text: app.tr("About")
            onTriggered: showPassiveNotification(app.tr("Action 2 clicked"))
        }
    }
}
