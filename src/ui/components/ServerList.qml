import QtQuick 2.7
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.3

import org.zap.gemby 1.0

import "../convergence-components/platform"

ColumnLayout {
    Controls.Frame {
        Layout.fillWidth: true

        PageListPL {
            width: parent.width
            height: parent.height
            clip: true

            model: Server {
                list: serverList
            }

            delegate: RowLayout {
                width: parent.width

                Controls.Button {
                    text: "Choose Server"
                }

                Controls.TextField {
                    text: model.url
                    Layout.fillWidth: true
                }
            }
        }
    }

    RowLayout {
        Controls.Button {
            text: "Add Server"
            onClicked: pageStack.push("qrc:///LoginPage.qml")
            Layout.fillWidth: true
        }
    }
}
