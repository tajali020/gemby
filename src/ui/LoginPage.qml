import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import org.zap.gemby 1.0

import "./convergence-components/platform"

PagePL {
    id: page

    Authentication {
        id: authentication
    }

    title: qsTr("Add Server")

    ColumnLayout {
        spacing: units.gu(2)
        width: page.width

        Item {
            Layout.fillHeight: true
        }

        TextField {
            id: serverField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Server")
        }

        TextField {
            id: usernameField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Username")
        }

        TextField {
            id: passwordField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            echoMode: TextInput.Password
            placeholderText: qsTr("Password")
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: 'Login!'
            onClicked: authentication.login(serverField.text, usernameField.text, passwordField.text)
        }

        Item {
            Layout.fillHeight: true
        }
    }
}
