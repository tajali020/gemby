import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import "./convergence-components/platform"
import "./convergence-components"
import "./components"

ApplicationWindowPL {
    id: app

    title: app.tr("gemby")
    initialPage: FirstPage { }
    StylerPL { id: styler }

    function tr(message) {
        // Return translated message.
        // In addition to the message, string formatting arguments can be passed
        // as well as short-hand for message.arg(arg1).arg(arg2)...
        message = qsTranslate("", message);
        for (var i = 1; i < arguments.length; i++)
            message = message.arg(arguments[i]);
        return message;
    }

    function push(pagefile, options, clearAll) {
        if (app.isConvergent && clearAll) {
            app.resetMenu();
            app.clearPages();
        }
        return app.pages.push(pagefile, options);
    }

    function pushAttached(pagefile, options) {
        return app.pages.pushAttached(pagefile, options);
    }

    function pushMain(pagefile, options) {
        // replace the current main with the new stack
        app._stackMain.clear();
        app.resetMenu();
        if (app.isConvergent) {
            app.clearPages();
        }
        return app._stackMain.push(pagefile, options);
    }

    function pushAttachedMain(pagefile, options) {
        // attach pages to the current main
        return app._stackMain.pushAttached(pagefile, options);
    }

    function resetMenu() {
        app._stackMain.keep = false;
        app.stateId = "";
        app.infoPanel.infoText = "";
    }
}
