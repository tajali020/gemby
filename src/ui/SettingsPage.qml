import QtQuick 2.0

import org.zap.gemby 1.0

import "./convergence-components/platform"

import "./components"

PagePL {
    id: page

    ServerList {
        width: page.width
    }
}
