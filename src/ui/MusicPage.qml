import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import org.zap.gemby 1.0

import "./convergence-components/platform"

PagePL {
    id: page

    ListView {
        // ...
        ScrollBar.horizontal: ScrollBar { }
        Button {
            text: "Songs"
        }
    }
}
