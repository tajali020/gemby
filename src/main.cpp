#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>

#include "backend/authentication.h"
#include "backend/song.h"
#include "backend/models/servermodel.h"

#include <QQmlContext>
#include "backend/models/serverlist.h"

#define GEMBY_URI "org.zap.gemby"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("gemby");

    qmlRegisterType<Authentication>(GEMBY_URI,1,0,"Authentication");
    qmlRegisterType<Song>(GEMBY_URI,1,0,"Song");
    qmlRegisterType<ServerModel>(GEMBY_URI,1,0,"Server");

    qmlRegisterUncreatableType<ServerList>(GEMBY_URI, 1, 0, "ServerList",
        QStringLiteral("ServerList should not be created in QML"));

    ServerList serverList;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty(QStringLiteral("serverList"), &serverList);

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
